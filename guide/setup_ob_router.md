author:            19A ITS1
summary:           Set up an openbsd router
id:                setup_ob_router
categories:        OpenBSD router example system
environments:      Virtual machines
status:            wip
feedback link:     MON@UCL.DK
analytics account: Always 0

# Set up an openbsd router

## Introduction

Following this guide, we have an OpenBSD router running as a VM on VMWare workstation. OpenBSD version used is 6.5.

The router will do NAT'ed routing between `vmnet10` and `vmnet8` (NAT).

IP addresses used will be
* vmnet8:  `192.168.203.0/24`
* vmnet10: `192.168.20.0/24`

## Download software

For the installation, we need the installation iso from openbsd.org.

We use the amd64 version, which will correspond to most current hypervisors on consumer hardware.

```
OB_VERSION="6.5"
OB_VERSION_NO_DOT="65"
DOWNLOADDIR="$HOME/Downloads/openbsd"

mkdir -p $DOWNLOAD_DIR
wget https://ftp.openbsd.org/pub/OpenBSD/$OB_VERSION/amd64/install${OB_VERSION_NO_DOT}.iso
```

and verify checksums
```
wget https://ftp.openbsd.org/pub/OpenBSD/$OB_VERSION/amd64/SHA256.sig
sha256sum -c SHA256.sig --ignore-missing
```
There will be a warning about the 2 lines, which is due to the .sig file signature at the top of the file.

## Create the VM

In this step we will create a minimal OpenBSD VM to be used for cloning.

In vmware workstation
1. Create new machine
2. Typical
3. Use ISO image and point to the `installXX.iso` you downloaded earlier
4. Guest operating system: `Other -> FreeBSD`

   It is the closest match since OpenBSD is not in the list.

5. Name the machine

   Suggested is something like `ob-base` to signal that it is not to be run, only cloned.

6. 20 GB of disk

The default with 256 MB of memory is sufficient.

Todo: We want to be able to do this from the command line.


## Basic installation

Default installation... ok, ok, next

We want to make an [asciinema](https://github.com/asciinema/asciinema) with the installation :-)

## Setup routing

We mostly follow [this example](https://www.openbsd.org/faq/pf/example1.html)

All is done as the root user

1. `echo 'net.inet.ip.forwarding=1' >> /etc/sysctl.conf`

   Enable routing persistently - will take effect after reboot.

2. `echo 'dhcp' > /etc/hostname.em0`

   Set the egress interface to DHCP

3.  `echo 'inet 192.168.10.2 255.255.255.0 192.168.10.255' > /etc/hostname.em1`

    Set inside IP address to `.2` to match what VMware usually does.

    The `.1` is the virtualisation host, if it has an interface on the vmnet.

4. Save [this file](etc_pf_conf.txt) as `/etc/pf.conf`

5. Reboot router

6. `ssh root@192.168.20.2`

   Access the router using `SSH`. This is the prefered way of adminsitrating the box


## Test

Put a test server on `vmnet10`:

* IP adress: `192.168.20.10`
* netmask: `255.255.255.0`
* Router: `192.168.20.2`
* DNS server: `192.168.20.2`

1. ping 8.8.8.8
2. ping google.com
